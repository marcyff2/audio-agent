﻿
using NUnit.Framework;
using System;

namespace Business.Test
{
    [TestFixture]
    public class ProcessUrlTest
    {
        IProcessUrl process { get; set; }

        [Test]
        public void EnterInvalidURL_GetInvalidUrlResponse()
        {
            process = new ProcessUrl();
            process.url = "httpss://google.com";
            Assert.IsTrue(!process.IsUrlValid());
        }
        [Test]
        public void EnterValidURL_GetValidUrlResponse()
        {
            ProcessUrl test = new ProcessUrl();
            test.url = "https://google.com";
            Assert.IsTrue(test.IsUrlValid());
        }
        [Test]
        public void URlIsValidImage_InvalidImage()
        {
            process = new ProcessUrl();
            process.url = "https://google.com";
            process.GetImage();
            Assert.IsTrue(!process.IsImageValid());
        }
        [Test]
        public void URlIsValidImage_ValidImage()
        {
            process = new ProcessUrl();
            process.url = "https://i.imgur.com/SxB7Dxu.jpg";
            process.GetImage();
            Assert.IsTrue(process.IsImageValid());
        }

        [Test]
        public void URIIsValidImage_GetDetails()
        {
            process = new ProcessUrl();
            process.url = "https://i.imgur.com/SxB7Dxu.jpg";
            Assert.IsTrue(!String.IsNullOrEmpty(process.error));
        }
        [Test]
        public void URIIsInvalidImage_GetDetails()
        {
            process = new ProcessUrl();
            process.url = "http://google.com";
            process.GetImageDetails();
            Assert.AreEqual(process.error, "The url is not a valid image. Please try again");
        }
        [Test]
        public void InvalidURL_GetDetails()
        {
            process = new ProcessUrl();
            process.url = "httgp://google.com";
            process.GetImageDetails();
            Assert.AreEqual(process.error, "The url is not valid. Please try again");
        }

    }
}
