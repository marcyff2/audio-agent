﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public interface IProcessUrl
    {
        string error { get; set; }
        string url { get; set; }
        Image image { get; set; }
        bool IsUrlValid();
        void GetImage();
        bool IsImageValid();
        void GetImageDetails();
    }
}
