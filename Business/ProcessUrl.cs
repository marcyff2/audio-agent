﻿using System;
using System.Drawing;
using System.IO;
using System.Net;

namespace Business
{
    public class ProcessUrl : IProcessUrl
    {
        public string error { get; set; }
        public Image image { get; set; }
        public string url { get; set; }

        public ProcessUrl()
        {

        }

        public bool IsUrlValid()
        {

            Uri uriResult;
            return Uri.TryCreate(url, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        public void GetImage()
        {
            try
            {

                string path = @"" + url;
                byte[] imageData = new WebClient().DownloadData(path);
                MemoryStream imgStream = new MemoryStream(imageData);
                image = Image.FromStream(imgStream);

            }
            catch (Exception)
            {
                image = null;
            }
        }

        public bool IsImageValid()
        {
            return (image != null);

        }

        public void GetImageDetails()
        {
           if(IsUrlValid())
            {
                GetImage();
                if (!IsImageValid())
                {
                    error = "The url is not a valid image. Please try again";
                }
            }
            else
            {
                error = "The url is not valid. Please try again";
            }
        }
    }
}
