﻿using ImageReading.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ImageReading.MVC.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string ProcessImage(string url)
        {
            HttpWebRequest request = WebRequest.Create("Http://localhost:63657/image/?url="+ url) as HttpWebRequest;
            request.Method = "GET";
            request.ContentType = "text/html";
            string results = string.Empty;
            HttpWebResponse response;
            using (response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                results = reader.ReadToEnd();
            }
            ImageDetailsModel model;
            results = results.Replace("\"","");
            return results;
        }
    }
}