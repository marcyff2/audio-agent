﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace ImageReading.Models
{
    public class ImageDetailsModel
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public float PhysicalHeight { get; set; }
        public float PhysicalWidth { get; set; }
        public float VerticalResolution { get; set; }
        public float HorizontalResolution { get; set; }
        public int Flags { get; set; }
        public string PixelFormat { get; set; }
        public ImageDetailsModel()
        {

        }
        public ImageDetailsModel(Image image)
        {
            Height = image.Height;
            Width = image.Width;
            VerticalResolution = image.VerticalResolution;
            HorizontalResolution = image.HorizontalResolution;
            PhysicalHeight = image.PhysicalDimension.Height;
            PhysicalWidth = image.PhysicalDimension.Width;
            Flags = image.Flags;
            PixelFormat =  image.PixelFormat.ToString();
        }
    }
}