﻿using ImageRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Drawing;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;

namespace ImageRestAPI.Controllers
{
    public class ImageController : ApiController
    {
        // GET api/<controller>/http://localhost:63657/image/?url=https%3A%2F%2Fs.imgur.com%2Fimages%2F404%2Fgiraffeweyes.png
        public string Get(string url)
        {
            ImageModel Model = new ImageModel(url);
            Model.ProcessInfo();
            if(Model.image == null) // in case the url or the image is not valid
                return Model.error;
            return new JavaScriptSerializer().Serialize(Model.image); ;
        }
    }
}