﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using ImageReading.Models;

namespace ImageRestAPI.Models
{
    public class ImageModel
    {

        public IProcessUrl process { get; set; }
        public ImageDetailsModel image { get; set; }
        public string error { get; set; }

        public ImageModel(string _Url)
        {
            process = new ProcessUrl();
            process.url = _Url;
        }
        public void ProcessInfo()
        {
            process.GetImageDetails();
            if (process.image == null)
                error = process.error;
            image = new ImageDetailsModel(process.image);
        }
    }
}